module gitlab.com/seide/cdhfa

go 1.12

require (
	github.com/manifoldco/promptui v0.3.2
	github.com/mattn/go-sqlite3 v1.11.0
	github.com/pkg/errors v0.8.1 // indirect
	github.com/pressly/goose v2.6.0+incompatible
)
