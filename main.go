package main

import (
	"flag"
	"fmt"
	"os"

	"github.com/manifoldco/promptui"
	_ "github.com/mattn/go-sqlite3"
)

var (
	appVersion   string
	appBuildDate string
	appOS        string
)

func main() {
	var pattern string

	addMode := flag.Bool("add", false, "adds current working directory to database")
	version := flag.Bool("version", false, "show build application properties")
	devFlag := flag.Bool("dev", false, "enables dev mode")

	flag.StringVar(&pattern, "search", "*", "search given pattern in your database")
	flag.Parse()

	if *version {
		fmt.Printf("Version: %s\n", appVersion)
		fmt.Printf("BuildDate: %s\n", appBuildDate)
		fmt.Printf("OS: %s\n", appOS)
		os.Exit(0)
	}

	db := NewDBClient(*devFlag)

	dir, err := os.Getwd()
	panicOnErr(err)
	pathHistory := NewPathHistory(dir)

	if *addMode {
		if err := pathHistory.Update(db); err != nil {
			if err := pathHistory.Create(db); err != nil {
				panic(err)
			}
		}
	} else if pattern != "*" {
		items, err := GetPathsByPattern(db, pattern)
		if err != nil {
			panic(err)
		}
		if len(items) == 0 {
			fmt.Printf("no paths with pattern %s are available\n", pattern)
		} else {
			err = usePromptUI(items)
			panicOnErr(err)
		}
	} else {
		items, err := GetPathsByUsage(db)
		if err != nil {
			panic(err)
		}
		err = usePromptUI(items)
		panicOnErr(err)
	}
}

func usePromptUI(items []string) error {
	prompt := promptui.Select{
		Label: "Pick a directory",
		Items: items,
	}

	_, result, err := prompt.Run()
	if err != nil {
		return err
	}

	fmt.Fprintf(os.Stderr, "%s\n", result)
	return nil
}

func panicOnErr(err error) {
	if err != nil {
		panic(err)
	}
}
