package main

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/user"
	"path"
	"time"

	"github.com/pressly/goose"
)

// DBMethods provides interface for database methods
type DBMethods interface {
	GetHitsByName(string) (int64, error)
	Create(PathHistory) error
	Update(PathHistory) error
	GetPathsByPattern() ([]string, error)
	GetPathsByUsage() ([]string, error)
	Close() error
}

// DB handles datbase client connection
type DB struct {
	Client *sql.DB
}

// NewDBClient creates database structure
func NewDBClient(devFlag bool) *DB {
	log.SetOutput(ioutil.Discard)
	db, err := sql.Open("sqlite3", dbPath(devFlag))
	if err != nil {
		panic(fmt.Sprintf("Couldn't open %s: %v", dbPath(devFlag), err))
	}

	goose.SetDialect("sqlite3")
	if err := goose.Up(db, "/opt/cdhist/migrations"); err != nil {
		panic(fmt.Sprintf("Couldn't migrate database: %v", err))
	}

	return &DB{db}
}

// GetHitsByName return hits for given path
func (db *DB) GetHitsByName(path string) (int64, error) {
	var hits int64
	err := db.Client.QueryRow(`SELECT "hits" FROM "cd_history" where path=?`, path).Scan(&hits)
	return hits, err
}

// Create creates new PathHistory entry
func (db *DB) Create(ph *PathHistory) error {
	var err error

	statement, err := db.Client.Prepare(`INSERT INTO "cd_history" (path, unix_timestamp, hits) VALUES (?,?,?)`)
	if err != nil {
		return err
	}
	_, err = statement.Exec(&ph.Path, &ph.UnixTimestamp, &ph.Hits)
	if err != nil {
		return err
	}
	return nil
}

// Update udates database entry (timestamp, hits)
func (db *DB) Update(ph *PathHistory) error {
	var err error

	statement, err := db.Client.Prepare(`UPDATE "cd_history" SET unix_timestamp=?, hits=? where path=?`)
	if err != nil {
		return fmt.Errorf("couldn't prepare update statement: %v", err)
	}
	_, err = statement.Exec(ph.UnixTimestamp, ph.Hits, ph.Path)
	if err != nil {
		return fmt.Errorf("couldn't execute update statement: %v", err)
	}
	return nil
}

// GetPathsByUsage returns the 10 most favorite paths
func (db *DB) GetPathsByUsage() ([]string, error) {
	rows, err := db.Client.Query(`
		SELECT
			path
		FROM
			(SELECT
				path,
				((?-unix_timestamp)/hits) as key
			FROM
				"cd_history"
			ORDER BY
				key
			LIMIT 10);`, time.Now().Unix())
	if err != nil {
		return []string{}, err
	}

	return extractPaths(rows)
}

// GetPathsByPattern returns the most favorite paths which contains given pattern
func (db *DB) GetPathsByPattern(pattern string) ([]string, error) {
	query := `SELECT path FROM "cd_history" WHERE path LIKE ?`
	rows, err := db.Client.Query(query, fmt.Sprintf("%%%s%%", pattern))
	if err != nil {
		return []string{}, err
	}

	return extractPaths(rows)

}

// Close closes db client connection
func (db *DB) Close() error {
	return db.Client.Close()
}

func extractPaths(rows *sql.Rows) ([]string, error) {
	var (
		err   error
		paths []string
	)
	for rows.Next() {
		var path string
		err = rows.Scan(&path)
		if err != nil {
			return []string{}, err
		}
		paths = append(paths, path)
	}

	return paths, nil
}

func dbPath(devFlag bool) string {
	var (
		err    error
		dbFile string
		appDir string
	)

	usr, err := user.Current()
	panicOnErr(err)

	dbFile = "cdhistory.db"
	appDir = fmt.Sprintf("%s/.config/", usr.HomeDir)
	if devFlag {
		appDir = "./"
	}
	err = os.MkdirAll(appDir, 0600)
	panicOnErr(err)

	return path.Join(appDir, dbFile)
}
