GIT_VERSION=$(shell git describe --abbrev=4 --dirty --always --tags)
DATE=$(shell date +%FT%T%z)
GOOS?=$(shell uname -s | tr '[:upper:]' '[:lower:]')

LDFLAGS=-ldflags "-w -s -X main.appVersion=${GIT_VERSION} -X main.appBuildDate=${DATE} -X main.appOS=${GOOS}"

build:
	go build ${LDFLAGS} -o cdhist

install:
	@mkdir -p /opt/cdhist
	@cp cdhist /opt/cdhist/
	@cp -r migrations /opt/cdhist/
	@chmod -R 0777 /opt/cdhist
