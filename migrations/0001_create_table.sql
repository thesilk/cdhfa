-- +goose Up
CREATE TABLE "cd_history" (path text, unix_timestamp int, hits int);
CREATE UNIQUE INDEX idx ON "cd_history" (path);
