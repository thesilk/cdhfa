# cd-history

`cdhist` is a small application which stores the most usage working directories. So it is easy to switch very fast to your most used workspaces. Also it is possible to search your workspace collection.

```
seide:/gitlab/cdhist $ ./cdhist -h
Usage of ./cdhist:
  -add
          adds current working directory to database
  -dev
          enables dev mode
  -search string
          search given pattern in your database (default "*")
  -version
          show build application properties<Plug>CocRefresh
```

If you use the dev mode a database in `cdhist` working directory will be created.


## Using cdhist

```
seide:/gitlab/cdhist $ ./cdhist
Use the arrow keys to navigate: ↓ ↑ → ←
? Pick a directory:
    /gitlab/cdhist
  ▸ /tmp

```

## Setup

Add following to your `.bashrc` and define a function to use `cdhist` in production mode.

```
export PS1='\$(cdhist -add)\u:\w\$ '
function c() {
	dir=$(cdhist 3>&1 1>&2 2>&3);
	cd $dir;
}
```

Also add `cdhist` after installing
```
make build
sudo make install
```
to your path variable.
