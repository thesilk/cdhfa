package main

import (
	"fmt"
	"time"
)

// PathHistory element of cdhist
type PathHistory struct {
	Path          string
	UnixTimestamp int64
	Hits          int64
	DB
}

// NewPathHistory contructor of PathHistory
func NewPathHistory(path string) *PathHistory {
	return &PathHistory{
		Path:          path,
		UnixTimestamp: time.Now().Unix(),
	}
}

// GetHitsByPath find history data for specific path
func (ph *PathHistory) GetHitsByPath(db *DB) error {
	var err error

	ph.Hits, err = db.GetHitsByName(ph.Path)
	if err != nil {
		return err
	}
	return nil
}

// Create inserts PathHistory entry in database
func (ph *PathHistory) Create(db *DB) error {
	ph.Hits++
	return db.Create(ph)
}

// Update PathHistory element
func (ph *PathHistory) Update(db *DB) error {
	if err := ph.GetHitsByPath(db); err != nil {
		return fmt.Errorf("couldn't get hits number of %s: %v", ph.Path, err)
	}

	ph.Hits++
	if err := db.Update(ph); err != nil {
		return err
	}
	return nil
}

// GetPathsByUsage returns list of Paths
func GetPathsByUsage(db *DB) ([]string, error) {
	return db.GetPathsByUsage()
}

// GetPathsByPattern returns list of path which hits pattern
func GetPathsByPattern(db *DB, pattern string) ([]string, error) {
	return db.GetPathsByPattern(pattern)
}
